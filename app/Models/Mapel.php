<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class mapel extends Model
{
    use HasFactory;

    protected $table = 'mapel';
    protected $fillable = ['id_mapel', 'nama', 'kode'];

    public function mengajar(){
        return $this->hasMany(Mengajar::class, 'id_kelas');
    }
}
