<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class kelas extends Model
{
    use HasFactory;

    protected $table = 'kelas';
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'kode', 'angkatan', 'jurusan', 'rombel'];

    public function siswa(){
        return $this->hasMany(Siswa::class);
    }
    public function mengajar(){
        return $this->hasMany(Mengajar::class, 'id_kelas');
    }
}
