<?php

namespace App\Http\Controllers;

use App\Models\guru;
use App\Models\Kelas;
use App\Models\Mapel;
use App\Models\Siswa;
use App\Models\Mengajar;
use App\Imports\GuruImport;
use App\Imports\KelasImport;
use App\Imports\MapelImport;
use App\Imports\SiswaImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
use DB;

class KurikulumController extends Controller
{
    public function pilihSiswa()
    {
        $siswa = Siswa::all();
        return view('kurikulum.pilih', [
            'title' => 'Pilih Siswa'
        ], compact('siswa'));
    }

    public function tampilRaport(Siswa $siswa)
    {
        return view('kurikulum.raport', [
            'murid' => $siswa,
            'title' => 'Raport Siswa'
        ]);
    }
    public function dashboard()
    {
        $siswa = Siswa::count();
        $guru  = guru::count();
        $mapel = mapel::count();
        $kelas = Kelas::count();
        return view('kurikulum.dashboard', [
            'title' => 'Dashboard'
        ], compact(['siswa', 'guru', 'mapel', 'kelas']));
    }

    // Siswa
    public function data_siswa()
    {
        $siswa = Siswa::all();
        $kelas = Kelas::all();
        return view('kurikulum.siswa', [
            'title' => 'Data Siswa'
        ], compact('siswa', 'kelas'));
    }
    public function tambah_siswa(Request $request)
    {   
        DB::table('siswa')->insert([
            'nis'  =>  $request->nis,
            'nama' => $request->nama,
            'alamat' => $request->alamat,
            'id_kelas' => $request->id_kelas,
            'gender' => $request->gender,
        ]);
 
        return redirect('/kurikulum/data_siswa')->with('sukses', 'Data berhasil ditambahkan');
    }
    public function edit_siswa(Request $request, $id)
    {   
        $siswa = Siswa::find($id);
        $siswa->update($request->all());
        return redirect('/kurikulum/data_siswa')->with('sukses', 'Berhasil Diupdate');
    }
    public function hapus_siswa($id)
    {   
        $siswa = Siswa::find($id);
        $siswa->delete($id);
        return redirect('/kurikulum/data_siswa')->with('sukses', 'Berhasil Dihapus');
    }


    // Guru
    public function data_guru()
    {
        $guru = Guru::all();
        $kelas = Kelas::all();
        $mapel = Mapel::all();
        return view('kurikulum.guru.guru', [
            'title' => 'Data Guru'
        ], compact('guru', 'kelas', 'mapel'));
    }
    public function tambah_guru(Request $request)
    {   
        $validatedData = $request->validate([
            'nip' => 'required|max:255',
            'username' => 'required|max:255',
            'level' => 'required',
            'password' => 'required',
            'nama' => 'required|max:255',
            'gender' => 'required',
            'alamat' => 'required'
        ]);
 
        $validatedData['password'] = Hash::make($validatedData['password']);
 
        Guru::create($validatedData);
        return redirect('/kurikulum/data_guru')->with('success', 'Data berhasil Ditambah');
    }
    public function edit_guru(Request $request, $id)
    {   
        $guru = Guru::find($id);
        $guru->update($request->all());
        return redirect('/kurikulum/data_guru')->with('sukses', 'Berhasil Diupdate');
    }
    public function hapus_guru($id)
    {   
        $guru = Guru::find($id);
        $guru->delete($id);
        return redirect('/kurikulum/data_guru')->with('sukses', 'Berhasil Dihapus');
    }
    public function detail_guru($nip){
        $guru = Guru::where('nip', $nip)->with('kelas','mapel')->first();
        $kelas = Kelas::all();
        $mengajar = Mengajar::all();
        $mapel = Mapel::all();
        return view('kurikulum.guru.detail', [
            'title' => 'Detail Guru'
        ], compact('guru', 'kelas', 'mapel', 'mengajar'));
    }
    public function tambah_transaksi(Request $request, $nip){
        DB::table('mengajar')->insert([
            'nip'  =>  $request->nip,
            'id_kelas' => $request->id_kelas,
            'id_mapel' => $request->id_mapel,
        ]);
        return redirect()->back()->with('sukses', 'Data berhasil ditambahkan');
    }
    public function edit_transaksi(Request $request, $id)
    {   
        $guru = Mengajar::find($id);
        $guru->update($request->all());
        return redirect()->back()->with('sukses', 'Berhasil Diupdate');
    }
    public function hapus_transaksi($id)
    {   
        $mengajar = Mengajar::find($id);
        $mengajar->delete($id);
        return redirect()->back()->with('sukses', 'Berhasil Dihapus');
    }

    // Matapelajaran
    public function data_mapel()
    {
        $mapel = Mapel::all();
        return view('kurikulum.mapel', [
            'title' => 'Mata Pelajaran'
        ], compact('mapel'));
    }
    public function tambah_mapel(Request $request)
    {   
        DB::table('mapel')->insert([
            'kode'  =>  $request->kode,
            'nama' => $request->nama,
        ]);
 
        return redirect('/kurikulum/data_mapel')->with('sukses', 'Data berhasil ditambahkan');
    }
    public function edit_mapel(Request $request, $id)
    {   
        $mapel = Mapel::find($id);
        $mapel->update($request->all());
        return redirect('/kurikulum/data_mapel')->with('sukses', 'Berhasil Diupdate');
    }
    public function hapus_mapel($id)
    {   
        $mapel = Mapel::find($id);
        $mapel->delete($id);
        return redirect('/kurikulum/data_mapel')->with('sukses', 'Berhasil Dihapus');
    }

    // Kelas
    public function data_kelas()
    {
        $kelas = kelas::all();
        return view('kurikulum.kelas', [
            'title' => 'Data Kelas'
        ], compact('kelas'));
    }
    public function tambah_kelas(Request $request)
    {   
        DB::table('kelas')->insert([
            'kode'  =>  $request->kode,
            'angkatan' => $request->angkatan,
            'jurusan' => $request->jurusan,
            'rombel' => $request->rombel,
        ]);
 
        return redirect('/kurikulum/data_kelas')->with('sukses', 'Data berhasil ditambahkan');
    }
    public function edit_kelas(Request $request, $id)
    {   
        $kelas = Kelas::find($id);
        $kelas->update($request->all());
        return redirect('/kurikulum/data_kelas')->with('sukses', 'Berhasil Diupdate');
    }
    public function hapus_kelas($id)
    {   
        $kelas = Kelas::find($id);
        $kelas->delete($id);
        return redirect('/kurikulum/data_kelas')->with('sukses', 'Berhasil Dihapus');
    }


    // Import Excel
    public function siswaImport(Request $request)
    {
        // validasi
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);

        // menangkap file excel
        $file = $request->file('file');

        // membuat nama file unik
        $nama_file = rand() . $file->getClientOriginalName();

        // upload ke folder file_siswa di dalam folder public
        $file->move('file_siswa', $nama_file);

        // import data
        Excel::import(new SiswaImport, public_path('/file_siswa/' . $nama_file));

        // notifikasi dengan session
        Session::flash('sukses', 'Data Siswa Berhasil Diimport!');

        // alihkan halaman kembali
        return redirect('/kurikulum/data_siswa');
    }

    public function guruImport(Request $request)
    {
        // validasi
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);

        // menangkap file excel
        $file = $request->file('file');

        // membuat nama file unik
        $nama_file = rand() . $file->getClientOriginalName();

        // upload ke folder file_siswa di dalam folder public
        $file->move('file_guru', $nama_file);

        // import data
        Excel::import(new GuruImport, public_path('/file_guru/' . $nama_file));

        // notifikasi dengan session
        Session::flash('sukses', 'Data Siswa Berhasil Diimport!');

        // alihkan halaman kembali
        return redirect('/kurikulum/data_guru');
    }

    public function mapelImport(Request $request)
    {
        // validasi
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);

        // menangkap file excel
        $file = $request->file('file');

        // membuat nama file unik
        $nama_file = rand() . $file->getClientOriginalName();

        // upload ke folder file_siswa di dalam folder public
        $file->move('file_mapel', $nama_file);

        // import data
        Excel::import(new MapelImport, public_path('/file_mapel/' . $nama_file));

        // notifikasi dengan session
        Session::flash('sukses', 'Data Siswa Berhasil Diimport!');

        // alihkan halaman kembali
        return redirect('/kurikulum/data_mapel');
    }

    public function kelasImport(Request $request)
    {
        // validasi
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);

        // menangkap file excel
        $file = $request->file('file');

        // membuat nama file unik
        $nama_file = rand() . $file->getClientOriginalName();

        // upload ke folder file_siswa di dalam folder public
        $file->move('file_kelas', $nama_file);

        // import data
        Excel::import(new KelasImport, public_path('/file_kelas/' . $nama_file));

        // notifikasi dengan session
        Session::flash('sukses', 'Data Siswa Berhasil Diimport!');

        // alihkan halaman kembali
        return redirect('/kurikulum/data_kelas');
    }
}
