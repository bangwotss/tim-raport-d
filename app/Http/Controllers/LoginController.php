<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\FlareClient\View;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function viewlogin()
    {
        return view('login', [
            'title' =>  'Login Raport Diagnostik'
        ]);
    }

    public function postlogin(request $request)
    {
        // if (Auth::attempt($request->only('username', 'password'))) {
        if (Auth::guard('guru')->attempt([
            'username' => $request->username, 'password' => $request->password])) {
             $request->session()->regenerate();
            if (\Auth::guard('guru')->user()->level == 'kurikulum') {
                return redirect('/kurikulum');
            } else if (\Auth::guard('guru')->user()->level == 'siswa') {
                return redirect('/siswa/dashboard');
            } else if (\Auth::guard('guru')->user()->level == 'walikelas') {
                return redirect('/walikelas/dashboard');
            } else if (\Auth::guard('guru')->user()->level == 'guru') {
                return redirect('/guru');
            } else {
                return redirect('/');
            }
        } else {
            return redirect('/login');
        }
    }

    public function logout(Request $request)
    {   
        Auth::logout();
        request()->session()->invalidate();
        request()->session()->regenerateToken();
        return redirect('/login');
    }
}
