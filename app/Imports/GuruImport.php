<?php

namespace App\Imports;

use App\Models\guru;
use Maatwebsite\Excel\Concerns\ToModel;

class GuruImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new guru([
            'nip' => $row[0],
            'nama' => $row[1],
            'id_mapel' => $row[2],
            'id_kelas' => $row[3],
            'alamat' => $row[4],
            'gender' => $row[5],
        ]);
    }
}
