@extends('guru.template.main')
@section('content')
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<div class="container">
	<button type="button" class="btn btn-primary  btn-icon-split" data-toggle="modal" data-target="#importExcel">
			<span class="icon text-white-50">
			<i class="fas fa-file-upload"></i></span>
			<span class="text">
			Excel
			</span>
		</button>

		<button type="button" class="btn btn-primary  btn-icon-split" data-toggle="modal" data-target="#tambahKelas">
			<span class="icon text-white-50">
			<i class="fa fa-plus"></i></span>
			<span class="text">
			Tambah
			</span>
		</button>
<hr> 
	
		<!-- Tambah -->
		<div class="modal fade" id="tambahKelas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<form method="post" action="/guru/tambahkomponen">
					<div class="modal-content border-left-primary">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Tambah Kelas</h5>
						</div>
						<div class="modal-body">
							@csrf
  						<div class="input-group mb-3">
 						<select class="form-control mr-3" name="mapel" aria-describedby="button-addon2">
 							@foreach($guru->mengajar as $mr)
                              	  <option value="" selected>{{$mr->id_kelas}}</option>
                            @endforeach
                              </select>
							</div>
							<div class="input-group field_wrapper mb-3">
								<input type="text" class="form-control mr-3" aria-describedby="button-addon2" name="nama_komponen[]">
								<a class="btn btn-primary" href="javascript:void(0);" id="add_button" title="Add field"><i class="fas fa-plus"></i></a>
							</div>
						</div>

						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
							<button type="submit" class="btn btn-primary">Import</button>
						</div>
					</div>
				</form>
			</div>
		</div>


		<!-- Import Excel -->
		<div class="modal fade" id="importExcel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<form method="post" action="/data_siswa_import" enctype="multipart/form-data">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Import Excel</h5>
						</div>
						<div class="modal-body"> 
							
							@csrf
							
							<div class="form-group">
								<input type="file" name="file" required="required">
							</div>
 
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary">Import</button>
						</div>
					</div>
				</form>
			</div>
		</div>

 		<!-- Y Table (Proses Client) -->
 		<div class="card shadow mb-4 mt-3">
      <div class="card border-left-dark shadow">
         <div class="card-header py-3">
            <h5 class="m-0 font-weight-bold">Data Siswa</h5>
         </div>
      <div class="card-body">

		<table id="myTable" class="table table-bordered">
			<thead>
				<tr>
					<th>Nomor Induk Siswa</th>
					<th>Nama</th>
					<th>Kode Kelas</th>
					<th>Alamat</th>
					<th>Gender</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				@php $i=1 @endphp
				@foreach($guru->mengajar as $mr)
				<tr>
					<td>{{$mr->id}}</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td> <button type="button" class="btn btn-warning btn-circle" data-toggle="modal" data-target="#editSiswa-"><i class="fas fa-edit"></i></button> | <button type="button" class="btn btn-danger btn-circle" data-toggle="modal" data-target="#hapusSiswa-"><i class="fas fa-trash"></i></button>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	 </div>
				</div>	
			</div>

			<script type="text/javascript">
$(document).ready(function(){
    var maxField = 10; //Input fields increment limitation
    var addButton = $('#add_button'); //Add button selector
    var wrapper = $('.field_wrapper'); //Input field wrapper
    var fieldHTML = '<div class="input-group mt-3">';
    fieldHTML=fieldHTML + '<input type="text" class="form-control mr-3" aria-describedby="button-addon2" name="nama_komponen[]">';
    fieldHTML=fieldHTML + '<div><a href="javascript:void(0);" class="remove_button btn btn-danger" ><i class="fa fa-minus"></i></a></div>';
    fieldHTML=fieldHTML + '</div></div>'; 
    var x = 1; //Initial field counter is 1
     
    //Once add button is clicked
    $(addButton).click(function(){
        //Check maximum number of input fields
        if(x < maxField){ 
            x++; //Increment field counter
            $(wrapper).append(fieldHTML); //Add field html
        }
    });
     
    //Once remove button is clicked
    $(wrapper).on('click', '.remove_button', function(e){
        e.preventDefault();
        $(this).parent('').parent('').remove(); //Remove field html
        x--; //Decrement field counter
    });
});
</script>

@endsection