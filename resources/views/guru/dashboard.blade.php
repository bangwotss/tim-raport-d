@extends('guru.template.main')
@section('content')
<h1 class="h3 mb-2 text-gray-800">Selamat Datang,</h1>
 <h5 class="h5 mb-2 text-gray-800">{{ auth()->user()->nip }}</h5>
 <hr>
  <div class="row align-items-start">
    <div class="col-xl-3 mb-3">
        <a href="/kurikulum/data_siswa" class="btn btn-secondary btn-icon-split">
           <span class="icon text-white-50">
             <i class="fas fa-arrow-right"></i>
           </span>
           <span class="text">Data siswa</span>
        </a>
    </div>
    <div class="col md-3">
        <a href="/kurikulum/data_guru" class="btn btn-secondary btn-icon-split">
           <span class="icon text-white-50">
             <i class="fas fa-arrow-right"></i>
           </span>
           <span class="text">Data guru</span>
        </a>
    </div>
  </div>
 <!-- Content Row -->
  <div class="row">
 <!-- Earnings (Monthly) Card Example -->
 <div class="col-xl-3 col-md-3 mb-4">
    <div class="card border-left-primary shadow h-100 py-2">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
              <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Siswa</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $komponen }}</div>
           </div>
                <div class="col-auto">
               <button type="button" class="btn btn-warning-sm btn-circle" data-toggle="modal" data-target="#editSiswa-"><i class="fas fa-edit"></i></button>
            </div>
         </div>
        </div>
    </div>
</div>
 <!-- Earnings (Monthly) Card Example -->
 <div class="col-xl-3 col-md-3 mb-4">
    <div class="card border-left-success shadow h-100 py-2">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
              <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Guru</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $komponen}}</div>
           </div>
                <div class="col-auto">
              <i class="fas fa-calendar fa-2x text-gray-300"></i>
            </div>
         </div>
        </div>
    </div>
</div>
</div>

<div class="card shadow mb-4">
  <div class="card-header py-3">
     <h6 class="m-0 font-weight-bold text-primary">Raport Diagnostik</h6>
         </div>
            <div class="card-body">
               <div class="text-center">
                   <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width: 25rem;" src="{{asset('template/img/undraw_posting_photo.svg')}}" alt="ilustrasi">
                </div>
                    <p>Add some quality, svg illustrations to your project courtesy of <a target="_blank" rel="nofollow" href="https://undraw.co/">unDraw</a>, a constantly updated collection of beautiful svg images that you can use completely free and without attribution!</p>
                </div>
         </div>
@endsection