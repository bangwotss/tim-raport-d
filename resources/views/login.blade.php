<!DOCTYPE html>
<html lang="en">

<head>
    <title>Raport Diagnostik - Login</title>
    @include('kurikulum.template.header')
</head>

<body style="background-color:#20201D;">

    <div class="container mt-5">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            <div class="col-xl-10 col-lg-12 col-md-9">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-block bg-login-image">
                            </div>
                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900">Selamat Datang</h1>
                                        <h5 class="h5 font-weight-bold text-primary">Raport Diagnostik</h5>
                                    </div>
                                    <hr>
                                    <form action="/postlogin" method="post">
                                        @csrf
                                            <div class="form-group mt-3">
                                                <input type="text" name="username" class="form-control" placeholder="Masukkan username anda" required>
                                            </div>
                                            <div class="form-group">
                                                 <input class="form-control" type="password" name="password" placeholder="Masukkan password anda" required>
                                            </div>
                                            <button type="submit" class="btn btn-primary btn-block">Login</button>
                                            <a href="/" class="btn btn-facebook btn-user btn-block">Halaman Utama</a>
                                        </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
<p>iygi</p>
<p>yfiyh</p>
        </div>

    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="assets/js/sb-admin-2.js"></script>
    <script src="assets/vendor/aos/aos.js"></script>
    <script src="assets/js/main.js"></script>
</body>

</html>