        <ul class="navbar-nav sidebar sidebar-dark accordion toggled" id="accordionSidebar" style="background-color:#20201D;">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/kurikulum">
                <div class="sidebar-brand-icon ">
                    <img src="{{asset('template/img/kemdikbud.png')}}" style="height: 50px;">
                </div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item {{ Request::is('kurikulum') ? 'active' : '' }}">
                <a class="nav-link" href="/kurikulum">
                    <i class="fa fa-coffee"></i>
                    <span>Home</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item {{ Request::is('kurikulum/data_siswa*') ? 'active' : '' }}">
                <a class="nav-link" href="/kurikulum/data_siswa">
                    <i class="fa fa-users"></i>
                    <span>Siswa</span>
                </a>
            </li>

            <!-- Nav Item - Utilities Collapse Menu -->
            <li class="nav-item {{ Request::is('kurikulum/data_guru*') ? 'active' : '' }}">
                <a class="nav-link" href="/kurikulum/data_guru">
                    <i class="fa fa-user"></i>
                    <span>Guru</span>
                </a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item {{ Request::is('kurikulum/data_mapel*') ? 'active' : '' }}">
                <a class="nav-link" href="/kurikulum/data_mapel">
                    <i class="fa fa-book"></i>
                    <span>Matapelajaran</span>
                </a>
            </li>

            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item {{ Request::is('kurikulum/data_kelas*') ? 'active' : '' }}">
                <a class="nav-link" href="/kurikulum/data_kelas">
                    <i class="fa fa-university"></i>
                    <span>Kelas</span>
                </a>
            </li>
            
            <li class="nav-item {{ Request::is('kurikulum/data_user*') ? 'active' : '' }}">
                <a class="nav-link" href="/kurikulum/data_user">
                    <i class="fas fa-fw fa-folder"></i>
                    <span>User</span>
                </a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">
        </ul>