@extends('kurikulum.template.main')

@section('content')

<div class="container">
<table class='table table-bordered mt-3'>
    <thead>
        <tr>
            <th>NIS</th>
            <th>ID_kelas</th>
            <th>Nama</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        @php $i=1 @endphp
        @foreach($siswa as $s)
        <tr>
            <td>{{ $s->nis }}</td>
            <td>{{$s->id_kelas}}</td>
            <td>{{$s->nama}}</td>
            <td><a href="{{ route('kurikulum.siswa', $s->nis) }}" class="btn btn-success">Tampil Raport</a></td>
        </tr>
        @endforeach
    </tbody>
</table>
</div>
@endsection