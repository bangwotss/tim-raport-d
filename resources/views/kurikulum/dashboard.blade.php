@extends('kurikulum.template.main')
@section('content')
<h1 class="h3 mb-2" style="color: #682773;">Selamat Datang,</h1>
 <h5 class="h5 mb-2 text-gray-800">{{ auth()->user()->nama }}</h5>
 <hr>
  <div class="container py-4">
    <div class="card-deck-wrapper">
        <div class="card-deck">
            <div class="card p-2">
                <a class="card-block stretched-link text-decoration-none" href="/kurikulum/data_siswa">
                    <center><h4 class="card-title"><i class="fa fa-users"></i></h4>
                    <p class="card-text">Siswa</p></center>
                </a>
            </div>
            <div class="card p-2">
                <a class="card-block stretched-link text-decoration-none" href="/kurikulum/data_guru">
                     <center><h4 class="card-title"><i class="fa fa-user"></i></h4>
                    <p class="card-text">Guru</p></center>
                </a>
            </div>
            <div class="card p-2">
                <a class="card-block stretched-link text-decoration-none" href="/kurikulum/data_mapel">
                     <center><h4 class="card-title"><i class="fa fa-book"></i></h4>
                    <p class="card-text">Matepelajaran</p></center>
                </a>
            </div>
            <div class="card p-2">
                <a class="card-block stretched-link text-decoration-none" href="/kurikulum/data_kelas">
                     <center><h4 class="card-title"><i class="fa fa-university"></i></h4>
                    <p class="card-text">Kelas</p></center>
                </a>
            </div>
        </div>
    </div>
</div>
                   <center><img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width: 25rem;" src="{{asset('template/img/undraw_engineering_team_a7n2.svg')}}" alt="ilustrasi">
                   <p>Add some quality, svg illustrations to your project courtesy of <a target="_blank" rel="nofollow" href="https:/kurikulum">K</a>, a constantly updated collection of beautiful svg images that you can use completely free and without attribution!</p></center> 

@endsection