@extends('kurikulum.template.main')

@section('content')
 
	<div class="container">
 
		{{-- notifikasi form validasi --}}
		@if ($errors->has('file'))
		<span class="invalid-feedback" role="alert">
			<strong>{{ $errors->first('file') }}</strong>
		</span>
		@endif
 
		{{-- notifikasi sukses --}}
		@if ($sukses = Session::get('sukses'))
		<div class="alert alert-success alert-block">
			<button type="button" class="close" data-dismiss="alert">×</button> 
			<strong>{{ $sukses }}</strong>
		</div>
		@endif
 
		<button type="button" class="btn btn-primary  btn-icon-split" data-toggle="modal" data-target="#importExcel">
			<span class="icon text-white-50">
			<i class="fas fa-file-upload"></i></span>
			<span class="text">
			Excel
			</span>
		</button>

		<button type="button" class="btn btn-outline-primary  btn-circle ml-2" data-toggle="modal" data-target="#tambahSiswa">
			<i class="fa fa-plus"></i>
		</button>
<hr>
		<!-- Tambah -->
		<div class="modal fade" id="tambahSiswa" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<form method="post" action="/kurikulum/tambah_siswa">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel" style="color: #682773;">Tambah Siswa</h5>
						</div>
						<div class="modal-body">
							@csrf
					 <div class="form-group row">
                        <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Nomor Induk Siswa</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="exampleInputUsername2" placeholder="Nomor Induk Siswa" name="nis" required>
                        </div>
                      </div>
 						
 					<div class="form-group row">
                        <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Nama</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="exampleInputUsername2" placeholder="Nama" name="nama" required>
                        </div>
                      </div>

                    	<div class="form-group row">
                            <label class="col-sm-3 col-form-label">Kode Kelas</label>
                            <div class="col-sm-9">
                              <select class="form-control" name="id_kelas">
                              	 @foreach ($kelas as $k)
                              	  <option value="{{$k->id}}" selected>{{ $k->kode }}</option>
  							     @endforeach
                              </select>
                            </div>
                          </div> 

                      <div class="form-group row">
                            <label class="col-sm-3 col-form-label ">Gender</label>
                            <div class="col-sm-4">
                              <div class="form-check">
                                <label class="form-check-label mt-3">
                                  <input type="radio" class="form-check-input" name="gender" id="membershipRadios1" value="L"> Laki <i class="input-helper"></i></label>
                              </div>
                            </div>
                            <div class="col-sm-5">
                              <div class="form-check">
                                <label class="form-check-label mt-3">
                                  <input type="radio" class="form-check-input" name="gender" id="membershipRadios2" value="P"> Perempuan <i class="input-helper"></i></label>
                              </div>
                            </div>
                          </div>

                      <div class="form-group row">
                        <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Alamat</label>
                        <div class="col-sm-9">
                        	<textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="alamat"></textarea>
                        </div>
                      </div>
     
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary">Import</button>
						</div>
					</div>
				</form>
			</div>
		</div>

		<!-- Import Excel -->
		<div class="modal fade" id="importExcel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<form method="post" action="/data_siswa_import" enctype="multipart/form-data">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Import Excel</h5>
						</div>
						<div class="modal-body"> 
							
							@csrf
							
							<div class="form-group">
								<input type="file" name="file" required="required">
							</div>
 
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary">Import</button>
						</div>
					</div>
				</form>
			</div>
		</div>

 		<!-- Y Table (Proses Client) -->
 		<div class="card shadow mb-4 mt-3">
      <div class="card border-left-dark shadow">
         <div class="card-header py-3">
            <h5 class="m-0 font-weight-bold">Data Siswa</h5>
         </div>
      <div class="card-body">

		<table id="myTable" class="table table-bordered">
			<thead>
				<tr>
					<th>Nomor Induk Siswa</th>
					<th>Nama</th>
					<th>Kode Kelas</th>
					<th>Alamat</th>
					<th>Gender</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				@php $i=1 @endphp
				@foreach($siswa as $s)
				<tr>
					<td>{{ $s->nis }}</td>
					<td>{{ $s->nama }}</td>
					<td>{{ $s->kelas->kode }}</td>
					<td>{{ $s->alamat }}</td>
					<td>{{ $s->gender }}</td>
					<td> <a class="btn btn-primary btn-circle" href="#" role="button"><i class="fa fa-id-card"></i></a> | <button type="button" class="btn btn-warning btn-circle" data-toggle="modal" data-target="#editSiswa-{{$s->nis}}"><i class="fas fa-edit"></i></button> | <button type="button" class="btn btn-danger btn-circle" data-toggle="modal" data-target="#hapusSiswa-{{$s->nis}}"><i class="fas fa-trash"></i></button>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	 </div>
				</div>	
			</div>

@foreach($siswa as $s)
<div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<form method="post" action="/kurikulum/tambah_siswa">
					<div class="modal-content border-left-primary">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Edit Siswa</h5>
						</div>
						<div class="modal-body">
							@csrf
					 <div class="form-group row">
                        <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Nomor Induk Siswa</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="exampleInputUsername2" placeholder="Nomor Induk Siswa" name="nis" required>
                        </div>
                      </div>
 						
 					<div class="form-group row">
                        <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Nama</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="exampleInputUsername2" placeholder="Nomor Induk Siswa" name="nama" required>
                        </div>
                      </div>

                    	<div class="form-group row">
                            <label class="col-sm-3 col-form-label">Kode Kelas</label>
                            <div class="col-sm-9">
                              <select class="form-control" name="id_kelas">
                              	 @foreach ($kelas as $k)
                              	  <option value="$s->id_kelas" selected>{{ $k->kode }}</option>
  							    						 @endforeach
                              </select>
                            </div>
                          </div> 

                      <div class="form-group row">
                            <label class="col-sm-3 col-form-label ">Gender</label>
                            <div class="col-sm-4">
                              <div class="form-check">
                                <label class="form-check-label mt-3">
                                  <input type="radio" class="form-check-input" name="gender" id="membershipRadios1" value="L"> Laki <i class="input-helper"></i></label>
                              </div>
                            </div>
                            <div class="col-sm-5">
                              <div class="form-check">
                                <label class="form-check-label mt-3">
                                  <input type="radio" class="form-check-input" name="gender" id="membershipRadios2" value="P"> Perempuan <i class="input-helper"></i></label>
                              </div>
                            </div>
                          </div>

                      <div class="form-group row">
                        <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Alamat</label>
                        <div class="col-sm-9">
                        	<textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="alamat"></textarea>
                        </div>
                      </div>
     
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
							<button type="submit" class="btn btn-primary">Simpan</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		@endforeach
 
@foreach($siswa as $s)
<div class="modal fade" id="editSiswa-{{$s->nis}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<form method="post" action="/kurikulum/{{$s->id}}/edit_siswa">
					<div class="modal-content border-left-primary">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Edit Siswa</h5>
						</div>
						<div class="modal-body">
							@csrf
					 <div class="form-group row">
                        <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Nomor Induk Siswa</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="exampleInputUsername2" placeholder="Nomor Induk Siswa" name="nis" value="{{$s->nis}}" required>
                        </div>
                      </div>
 						
 					<div class="form-group row">
                        <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Nama</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="exampleInputUsername2" placeholder="Nomor Induk Siswa" name="nama" value="{{$s->nama}}" required>
                        </div>
                      </div>

                    	<div class="form-group row">
                            <label class="col-sm-3 col-form-label">Kode Kelas</label>
                            <div class="col-sm-9">
                              <select class="form-control" name="id_kelas">
                              	 @foreach ($kelas as $k)
                              	 @if(old('id_kelas', $s->id_kelas) == $k->id)
                              	  <option value="{{ $k->id }}" selected>{{ $k->kode }}</option>
   															 @else
     							  						 <option value="{{ $k->id }}">{{ $k->kode }}</option>
  							   							 @endif
  							    						 @endforeach
                              </select>
                            </div>
                          </div> 

                      <div class="form-group row">
                            <label class="col-sm-3 col-form-label ">Gender</label>
                            <div class="col-sm-4">
                              <div class="form-check">
                                <label class="form-check-label mt-3">
                                  <input type="radio" class="form-check-input" name="gender" id="membershipRadios1" value="L" @if($s->gender == "L") checked @endif> Laki <i class="input-helper"></i></label>
                              </div>
                            </div>
                            <div class="col-sm-5">
                              <div class="form-check">
                                <label class="form-check-label mt-3">
                                  <input type="radio" class="form-check-input" name="gender" id="membershipRadios2" value="P" @if($s->gender == "P") checked @endif> Perempuan <i class="input-helper"></i></label>
                              </div>
                            </div>
                          </div>

                      <div class="form-group row">
                        <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Alamat</label>
                        <div class="col-sm-9">
                        	<textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="alamat" value="{{$s->alamat}}">{{$s->alamat}}</textarea>
                        </div>
                      </div>
     
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
							<button type="submit" class="btn btn-primary">Simpan</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		@endforeach

		@foreach($siswa as $s)
<div class="modal fade" id="hapusSiswa-{{$s->nis}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<form method="post" action="/kurikulum/{{$s->id}}/hapus_siswa">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Yakin akan hapus?</h5>
						</div>
						<div class="modal-body">
							@csrf
							Tekan "Simpan" untuk melanjutkan proses Anda saat ini.
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
							<button type="submit" class="btn btn-primary">Simpan</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		@endforeach

	@endsection