@extends('kurikulum.template.main')

@section('content')

<div class="container">

		{{-- notifikasi sukses --}}
		@if ($sukses = Session::get('sukses'))
		<div class="alert alert-success alert-block">
			<button type="button" class="close" data-dismiss="alert">×</button> 
			<strong>{{ $sukses }}</strong>
		</div>
		@endif

<div class="card shadow mb-4 mt-3">
      <div class="card border-left-dark shadow">
         <div class="card-header py-3">
            <h5 class="m-0 font-weight-bold">Detail Guru</h5>
         </div>
      <div class="card-body">
    <div class="row">
        <div class="col-md-5 border-right">
            <div class="p-3 py-5">
                <div class="d-flex justify-content-between align-items-center mb-3">
                    <h4 class="text-right" style="color: #682773;">Profil</h4>
                </div>
                <div class="row mt-3">
                	<div class="col-md-12"><label class="labels">Nama</label><input type="text" class="form-control" placeholder="education" value=""></div>
                	<div class="col-md-12"><label class="labels">Nomor Induk Pegawai</label><input type="text" class="form-control" placeholder="education" value=""></div>
                	  <div class="col-md-12"><label class="labels">Nama Pengguna</label><input type="text" class="form-control" placeholder="education" value=""></div>
                    <div class="col-md-12"><label class="labels">Email</label><input type="text" class="form-control" placeholder="enter email id" value=""></div>
                    <div class="col-md-12"><label class="labels">Alamat</label><input type="text" class="form-control" placeholder="enter address line 1" value=""></div>
                    <div class="row mt-3">
                    <div class="col-md-6"><label class="labels">Kecamatan</label><input type="text" class="form-control" placeholder="country" value=""></div>
                    <div class="col-md-6"><label class="labels">Kota</label><input type="text" class="form-control" value="" placeholder="state"></div>
                    <div class="col-md-6"><label class="labels">Kode Pos</label><input type="text" class="form-control" value="" placeholder="state"></div>
                </div>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="py-5">
            	<button type="button" class="btn btn-primary  btn-circle ml-2" data-toggle="modal" data-target="#tambahGuru">
			<i class="fa fa-plus"></i>
		</button>
		<hr>
               <table id="myTable" class="table table-bordered">
			<thead>
				<tr>
					<th>Kode</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			@foreach($guru->mengajar as $mr)	
				<tr>
					<td>{{ $mr->id }}</td>
					<td><button type="button" class="btn btn-warning btn-circle" data-toggle="modal" data-target="#editGuru-{{$mr->id}}"><i class="fas fa-edit"></i></button> | <button type="button" class="btn btn-danger btn-circle" data-toggle="modal" data-target="#hapusGuru-{{$mr->id}}"><i class="fas fa-trash"></i></button></td>
				</tr>
			@endforeach
			</tbody>
		</table>
            </div>
        </div>
    </div>
    </div>
				</div>	
			</div>

<div class="modal fade" id="tambahGuru" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<form method="post" action="/kurikulum/data_guru/tambahtransaksi_{{$guru->nip}}">
					<div class="modal-content border-left-primary">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Tambah Kelas</h5>
						</div>
						<div class="modal-body">
							@csrf
					  <div class="form-group row">
                        <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Nomor Induk Pegawai</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="exampleInputUsername2" placeholder="Nomor Induk Pegawai" name="nip" value="{{$guru->nip}}" required disabled>
                        </div>
                      </div>
						

                    	<div class="form-group row">
                            <label class="col-sm-3 col-form-label">Kode Kelas</label>
                            <div class="col-sm-9">
                              <select class="form-control" name="id_kelas">
                              	@foreach($kelas as $k)
                              	  <option value="{{$k->id}}" selected>{{ $k->kode }}</option>
                              	@endforeach
                              </select>
                            </div>
                          </div>  

                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Kode Kelas</label>
                            <div class="col-sm-9">
                              <select class="form-control" name="id_mapel">
                              	@foreach($mapel as $m)
                              	  <option value="{{ $m->id }}" selected>{{ $m->kode }}</option>
                              	@endforeach
                              </select>
                            </div>
                          </div>  	
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
							<button type="submit" class="btn btn-primary">Tambah</button>
						</div>
					</div>
				</form>
			</div>
		</div>

		@foreach($guru->mengajar as $mr)
				<div class="modal fade" id="hapusGuru-{{ $mr->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<form method="post" action="/kurikulum/data_guru/hapustransaksi_{{ $mr->id }}">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Yakin akan hapus?</h5>
						</div>
						<div class="modal-body">
							@csrf
							Tekan "Simpan" untuk melanjutkan proses Anda saat ini.
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
							<button type="submit" class="btn btn-primary">Simpan</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		@endforeach

			@foreach($guru->mengajar as $mr)
		<div class="modal fade" id="editGuru-{{$mr->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<form method="post" action="/kurikulum/data_guru/edittransaksi_{{ $mr->id }}">
					<div class="modal-content border-left-primary">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Edit</h5>
						</div>
						<div class="modal-body">
							@csrf
					  <div class="form-group row">
                        <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Nomor Induk Pegawai</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="exampleInputUsername2" placeholder="Nomor Induk Pegawai" name="nip" value="{{ $guru->nip }}" required>
                        </div>
                      </div>
						

                    	<div class="form-group row">
                            <label class="col-sm-3 col-form-label">Kode Kelas</label>
                            <div class="col-sm-9">
                              <select class="form-control" name="id_kelas">
                              	  @foreach ($kelas as $k)
                              	 @if(old('id_kelas', $mr->id_kelas) == $k->id)
                              	  <option value="{{ $k->id }}" selected>{{ $k->kode }}</option>
   															 @else
     							  						 <option value="{{ $k->id }}">{{ $k->kode }}</option>
  							   							 @endif
  							    						 @endforeach
                              </select>
                            </div>
                          </div>  

                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Kode Kelas</label>
                            <div class="col-sm-9">
                              <select class="form-control" name="id_mapel">
                              	 @foreach ($mapel as $m)
                              	 @if(old('id_mapel', $mr->id_mapel) == $m->id)
                              	  <option value="{{ $m->id }}" selected>{{ $m->nama }}</option>
   															 @else
     							  						 <option value="{{ $m->id }}">{{ $m->nama }}</option>
  							   							 @endif
  							    						 @endforeach
                              </select>
                            </div>
                          </div>  	

						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
							<button type="submit" class="btn btn-primary">Tambah</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		@endforeach

</div>

@endsection