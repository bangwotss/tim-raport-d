<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GuruController;
use App\Http\Controllers\HubinController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\SiswaController;
use App\Http\Controllers\WakelController;
use App\Http\Controllers\SekolahController;
use App\Http\Controllers\KurikulumController;
use App\Http\Controllers\PerusahaanController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\KomponenController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route Hubin */

Route::get('/', function () {
    return view('landing-page.index', [
        'title' =>  "Raport Diagnostik",
    ]);
});

Route::get('/about', function () {
    return view('landing-page.about', [
        'title' => "About Raport Diagnostik"
    ]);
});



/*Route Login*/
Route::get('/login', [LoginController::class, 'viewlogin'])->name('login');
Route::post('/postlogin', [LoginController::class, 'postlogin'])->name('postlogin');
Route::post('/logout', [LoginController::class, 'logout'])->name('logout');

/*Routes Kurikulum*/
Route::middleware(['auth:guru', 'ceklevel:kurikulum'])->group(function () {
    Route::get('/kurikulum', [KurikulumController::class, 'dashboard']);
    // Siswa
    Route::post('/data_siswa_import', [KurikulumController::class, 'siswaImport']);
    Route::get('/kurikulum/data_siswa', [KurikulumController::class, 'data_siswa']);
    Route::post('/kurikulum/tambah_siswa', [KurikulumController::class, 'tambah_siswa']);
    Route::post('/kurikulum/{id}/edit_siswa', [KurikulumController::class, 'edit_siswa']);
    Route::post('/kurikulum/{id}/hapus_siswa', [KurikulumController::class, 'hapus_siswa']);
    // Guru
    Route::post('/data_guru_import', [KurikulumController::class, 'guruImport']);
    Route::get('/kurikulum/data_guru', [KurikulumController::class, 'data_guru']);
    Route::get('/kurikulum/data_guru/detail_{id}', [KurikulumController::class, 'detail_guru']);
    Route::post('/kurikulum/data_guru/tambahtransaksi_{nip}', [KurikulumController::class, 'tambah_transaksi']);
    Route::post('/kurikulum/data_guru/edittransaksi_{id}', [KurikulumController::class, 'edit_transaksi']);
     Route::post('/kurikulum/data_guru/hapustransaksi_{id}', [KurikulumController::class, 'hapus_transaksi']);
    Route::post('/kurikulum/tambah_guru', [KurikulumController::class, 'tambah_guru']);
    Route::post('/kurikulum/{id}/edit_guru', [KurikulumController::class, 'edit_guru']);
    Route::post('/kurikulum/{id}/hapus_guru', [KurikulumController::class, 'hapus_guru']);
    // User
    Route::get('/kurikulum/data_user', [UserController::class, 'index']);
    Route::post('/data_user_import', [UserController::class, 'userImport']);
    // Matapelajaran
    Route::post('/data_mapel_import', [KurikulumController::class, 'mapelImport']);
    Route::get('/kurikulum/data_mapel', [KurikulumController::class, 'data_mapel']);
    Route::post('/kurikulum/tambah_mapel', [KurikulumController::class, 'tambah_mapel']);
    Route::post('/kurikulum/{id}/edit_mapel', [KurikulumController::class, 'edit_mapel']);
    Route::post('/kurikulum/{id}/hapus_mapel', [KurikulumController::class, 'hapus_mapel']);
    // Kelas
    Route::post('/data_kelas_import', [KurikulumController::class, 'kelasImport']);
    Route::get('/kurikulum/data_kelas', [KurikulumController::class, 'data_kelas']);
    Route::post('/kurikulum/tambah_kelas', [KurikulumController::class, 'tambah_kelas']);
    Route::post('/kurikulum/{id}/edit_kelas', [KurikulumController::class, 'edit_kelas']);
    Route::post('/kurikulum/{id}/hapus_kelas', [KurikulumController::class, 'hapus_kelas']);
    // Raport
    Route::get('/kurikulum/pilih_siswa', [KurikulumController::class, 'pilihSiswa']);
    Route::get('/kurikulum/raport_siswa/{siswa:nis}', [KurikulumController::class, 'tampilRaport'])->name('kurikulum.siswa');
}); //End

/*Routes Siswa*/
Route::middleware(['auth:guru', 'ceklevel:siswa'])->group(function () {
    route::get('/siswa/dashboard', [SiswaController::class, 'dashboard']);
});

/*Routes Walikelas*/
Route::middleware(['auth:guru', 'ceklevel:walikelas'])->group(function () {
    Route::get('/walikelas', [WakelController::class, 'dashboard']);
});

/*Routes Guru*/
Route::middleware(['auth:guru', 'ceklevel:guru'])->group(function () {
    Route::get('/guru', [GuruController::class, 'dashboard']);
    Route::get('/guru/komponen', [GuruController::class, 'komponen']);
    Route::post('/guru/tambahkomponen', [GuruController::class, 'proses']);
    Route::get('dinamis/form', [KomponenController::class, 'form']);
    Route::post('dinamis/proses', [KomponenController::class, 'proses']);
});
